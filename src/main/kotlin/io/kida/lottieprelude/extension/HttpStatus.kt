package io.kida.lottieprelude.extension

import io.kida.ktor.middleware.model.HttpStatus

fun HttpStatus.Companion.BadRequest(details: Any? = null): HttpStatus {
    return HttpStatus(400, "bad request", details)
}