package io.kida.lottieprelude.extension

import com.google.gson.Gson

fun Any.gsonify(): String {
    return Gson().toJson(this)
}