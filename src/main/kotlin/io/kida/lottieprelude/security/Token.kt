package io.kida.lottieprelude.security

import org.joda.time.DateTime
import java.util.*

class Token {
    companion object {
        val GRACE_TIME = 300 // seconds
        fun provision(): String {
            return Base64.getEncoder().encodeToString((DateTime().secondOfDay + GRACE_TIME).toString().toByteArray())
        }

        fun validate(token: String): Boolean {
            return String(Base64.getDecoder().decode(token)).toInt() > DateTime().secondOfDay().get()
        }
    }
}