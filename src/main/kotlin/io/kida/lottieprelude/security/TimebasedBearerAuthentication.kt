package io.kida.lottieprelude.security

import io.kida.ktor.middleware.Middleware
import io.kida.ktor.middleware.model.HttpStatus
import org.jetbrains.ktor.pipeline.PipelineContext

class TimebasedBearerAuthentication() : Middleware<Unit, HttpStatus> {
    suspend override fun pass(ctx: PipelineContext<Unit>): HttpStatus? {
        ctx.call.request.headers["Authorization"]?.split("Bearer ")?.last()?.let {
            if (Token.validate(it)) {
                return null
            }
        }
        return HttpStatus(401, "unauthorized")
    }
}