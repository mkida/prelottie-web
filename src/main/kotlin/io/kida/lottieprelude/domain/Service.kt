package io.kida.lottieprelude.domain

val BASE_URL: String? = System.getenv("BASE_URL")
val PORT: String? = System.getenv("PORT")

class Service {
    companion object {
        val port: Int = when {
            PORT != null -> PORT.toInt()
            else -> 8080
        }

        val baseUrl = when {
            BASE_URL != null -> BASE_URL
            else -> "http://localhost:$port"
        }
    }
}