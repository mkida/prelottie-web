package io.kida.lottieprelude.domain

import io.kida.lottieprelude.extension.gsonify
import io.kida.lottieprelude.model.Repository

class Bucket {
    companion object {
        val name = System.getenv("AWS_S3_BUCKET")
        fun putRepository(uuid: String, repository: Repository) {
            val repositoryJson = repository.gsonify()
            SpaceClient.minioClient.putObject(name, "$uuid.json", repositoryJson.byteInputStream(), "application/json")
        }
    }
}
