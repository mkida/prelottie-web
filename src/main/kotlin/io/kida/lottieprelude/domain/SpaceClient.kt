package io.kida.lottieprelude.domain

import io.minio.MinioClient

class SpaceClient {
    companion object {
        val minioClient = MinioClient(
                System.getenv("MINIO_ENDPOINT_URL"),
                System.getenv("MINIO_ACCESS_KEY"),
                System.getenv("MINIO_ACCESS_SECRET")
        )
    }
}