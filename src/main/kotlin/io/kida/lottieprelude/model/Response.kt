package io.kida.lottieprelude.model

data class Response(val session: String?, val status: String, val reason: String, val repository: Repository? = null)
