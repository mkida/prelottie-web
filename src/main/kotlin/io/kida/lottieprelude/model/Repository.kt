package io.kida.lottieprelude.model

typealias Url = String

data class File(val uri: String, val key: String)
data class Repository(val descriptor: Url? = null, val files: List<File>)

fun Repository.withoutDescriptor(): Repository {
    return Repository(null, files)
}