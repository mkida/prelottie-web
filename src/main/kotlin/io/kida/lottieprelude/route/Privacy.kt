package io.kida.lottieprelude.route

import org.jetbrains.ktor.freemarker.FreeMarkerContent
import org.jetbrains.ktor.response.respond
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get

fun Routing.privacy(): Route {
    return get("/privacy") {
        call.respond(FreeMarkerContent("privacy.ftl", emptyMap<String, String>()))
    }
}