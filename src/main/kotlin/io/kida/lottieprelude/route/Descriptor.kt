package io.kida.lottieprelude.route

import io.kida.lottieprelude.domain.Bucket
import io.kida.lottieprelude.domain.SpaceClient
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.response.respondWrite
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get

fun Routing.descriptor(): Route {
    return get("/descriptor/{id}") {
        call.respondWrite(ContentType.Application.Json) {
            SpaceClient.minioClient.getObject(Bucket.name, "${ call.parameters["id"]}.json")
                    .bufferedReader().forEachLine {
                write(it)
                flush()
            }
            close()
        }
    }
}
