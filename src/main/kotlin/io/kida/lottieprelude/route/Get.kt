package io.kida.lottieprelude.route

import org.jetbrains.ktor.response.respondRedirect
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get

fun Routing.get(): Route {
    return get("/get") {
//        call.respondRedirect("https://s.kida.io/lottie-prelude/app-release.apk")
        call.respondRedirect("https://play.google.com/store/apps/details?id=io.kida.lottieprelude")
    }
}