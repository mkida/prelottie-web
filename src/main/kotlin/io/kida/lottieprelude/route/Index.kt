package io.kida.lottieprelude.route

import org.jetbrains.ktor.freemarker.FreeMarkerContent
import org.jetbrains.ktor.response.respond
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get

fun Routing.index(): Route {
    return get("/") {
        call.respond(FreeMarkerContent("index.ftl", mapOf(
                "id" to null, "sampleId" to System.getenv("SAMPLE_ID")
        )))
    }
}