package io.kida.lottieprelude.route

import io.kida.ktor.middleware.model.HttpStatus
import io.kida.lottieprelude.domain.Bucket
import io.kida.lottieprelude.domain.SpaceClient
import io.kida.lottieprelude.extension.BadRequest
import io.kida.lottieprelude.extension.gsonify
import net.glxn.qrgen.core.image.ImageType
import net.glxn.qrgen.javase.QRCode
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.response.respond
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get
import java.io.InputStream

fun Routing.qr(): Route {
    return get("/qr/{id}") {

        val id = call.parameters["id"]
        var file: InputStream? = null

        if (id != null) {
            file = SpaceClient.minioClient.getObject(Bucket.name, "$id.json")
        }

        if (file != null) {
            call.respond(QRCode.from(call.parameters["id"]).withSize(512, 512).to(ImageType.PNG).stream().toByteArray())
        } else {
            call.respondText(HttpStatus.BadRequest().gsonify(), ContentType.Application.Json)
        }

    }
}