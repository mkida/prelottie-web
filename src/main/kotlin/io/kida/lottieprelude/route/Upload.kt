package io.kida.lottieprelude.route

import io.kida.ktor.middleware.Middleware
import io.kida.ktor.middleware.model.HttpStatus
import io.kida.lottieprelude.domain.Bucket
import io.kida.lottieprelude.domain.Service
import io.kida.lottieprelude.domain.SpaceClient
import io.kida.lottieprelude.extension.BadRequest
import io.kida.lottieprelude.extension.gsonify
import io.kida.lottieprelude.model.Repository
import io.kida.lottieprelude.model.Response
import io.kida.lottieprelude.model.withoutDescriptor
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.request.PartData
import org.jetbrains.ktor.request.header
import org.jetbrains.ktor.request.isMultipart
import org.jetbrains.ktor.request.receiveMultipart
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.post
import org.zeroturnaround.zip.ZipUtil
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import kotlin.collections.ArrayList


fun Routing.upload(): Route {
    return post("/upload") {

        val uuid = UUID.randomUUID().toString()

        call.request.header("Content-Length")?.let {
            if (it.toInt() > 1024 * 1024 * 5) {
                call.respondText(HttpStatus.BadRequest(
                        Response(uuid, "failure", "max file size exceeded")
                ).gsonify(), ContentType.Application.Json, HttpStatusCode.BadRequest)
                return@post
            }
        }

        if (!call.request.isMultipart()) {
            call.respondText(HttpStatus.BadRequest(
                    Response(uuid, "failure", "only multipart allowed")
            ).gsonify(), ContentType.Application.Json, HttpStatusCode.BadRequest)
            return@post
        }

        val parts = call.receiveMultipart().parts
                .filter { it is PartData.FileItem }
                .map { it as PartData.FileItem }

        if (!parts.any()) {
            call.respondText(HttpStatus.BadRequest(
                    Response(uuid, "failure", "no file parts present")
            ).gsonify(), ContentType.Application.Json, HttpStatusCode.BadRequest)
            return@post
        }

        if (parts.first().contentType == ContentType.Application.Zip) {
            val outDir = File("/tmp/" + uuid)
            ZipUtil.unpack(parts.first().streamProvider(), outDir)

            val files = ArrayList<io.kida.lottieprelude.model.File>()
            Files.walk(Paths.get(outDir.path))
                    .filter { Files.isRegularFile(it) }
                    .forEach {
                        val fileKey = Paths.get(Paths.get("/", "tmp", uuid).relativize(it.parent).toString(), it.fileName.toString()).toString()
                        System.out.println("PUT >> ${it.toFile().absolutePath} >> $uuid/$fileKey")
                        SpaceClient.minioClient.putObject(Bucket.name, "$uuid/$fileKey", it.toFile().absolutePath)
                        files.add(io.kida.lottieprelude.model.File("${Service.baseUrl}/file/$uuid?key=$fileKey", fileKey))
                    }

            val repository = Repository("${Service.baseUrl}/descriptor/$uuid", files)
            Bucket.putRepository(uuid, repository.withoutDescriptor())
            call.respondText(HttpStatus.OK(Response(uuid, "success", "Zip stored correctly.", repository)).gsonify(), ContentType.Application.Json)

        } else {
            val files = ArrayList<io.kida.lottieprelude.model.File>()
            parts.forEach {
                val streamProvider = it.streamProvider()
                val mimeType = it.contentType?.contentType ?: "application/octet-stream"
                SpaceClient.minioClient.putObject(Bucket.name, "$uuid/${it.originalFileName}", streamProvider, mimeType)
                System.out.println("PUT >> ${it.originalFileName} >> $uuid/${it.originalFileName}")
                files.add(io.kida.lottieprelude.model.File("${Service.baseUrl}/file/$uuid?key=${it.originalFileName}", it.originalFileName!!))
                it.dispose()
            }

            val repository = Repository("${Service.baseUrl}/descriptor/$uuid", files)
            Bucket.putRepository(uuid, repository.withoutDescriptor())
            call.respondText(HttpStatus.OK(Response(uuid, "success", "${parts.count()} items stored correctly.", repository)).gsonify(), ContentType.Application.Json)
        }
    }
}