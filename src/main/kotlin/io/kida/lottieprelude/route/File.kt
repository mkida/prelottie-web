package io.kida.lottieprelude.route

import io.kida.ktor.middleware.model.HttpStatus
import io.kida.lottieprelude.domain.Bucket
import io.kida.lottieprelude.domain.SpaceClient
import io.kida.lottieprelude.extension.BadRequest
import io.kida.lottieprelude.extension.gsonify
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.response.respond
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Route
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get
import java.io.InputStream

fun Routing.file(): Route {
    return get("/file/{id}") {
        val id = call.parameters["id"]
        val key = call.request.queryParameters["key"]
        var file: InputStream? = null

        if (id != null && key != null) {
            file = SpaceClient.minioClient.getObject(Bucket.name, "$id/$key")
        }

        if (file != null) {
            call.respond(file.readBytes())
        } else {
            call.respondText(HttpStatus.BadRequest().gsonify(), ContentType.Application.Json)
        }
    }
}
