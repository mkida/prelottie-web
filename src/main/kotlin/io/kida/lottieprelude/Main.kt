package io.kida.lottieprelude

import freemarker.cache.ClassTemplateLoader
import io.kida.ktor.middleware.BearerAuthentication
import io.kida.ktor.middleware.model.HttpStatus
import io.kida.lottieprelude.domain.Service
import io.kida.lottieprelude.extension.gsonify
import io.kida.lottieprelude.route.*
import io.kida.lottieprelude.security.TimebasedBearerAuthentication
import org.jetbrains.ktor.application.Application
import org.jetbrains.ktor.application.install
import org.jetbrains.ktor.content.defaultResource
import org.jetbrains.ktor.content.resources
import org.jetbrains.ktor.content.static
import org.jetbrains.ktor.features.ConditionalHeaders
import org.jetbrains.ktor.features.DefaultHeaders
import org.jetbrains.ktor.features.PartialContentSupport
import org.jetbrains.ktor.features.StatusPages
import org.jetbrains.ktor.freemarker.FreeMarker
import org.jetbrains.ktor.host.embeddedServer
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.netty.Netty
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Routing

fun Application.module() {
    install(DefaultHeaders)
    install(ConditionalHeaders)
    install(PartialContentSupport)
    install(StatusPages) {
        HttpStatusCode.allStatusCodes
            .filter { it !in listOf(
                    HttpStatusCode.OK,
                    HttpStatusCode.BadRequest,
                    HttpStatusCode.NotModified
            ) }
            .map { status(it) {
                call.respondText(HttpStatus(it.value, it.description).gsonify(), ContentType.Application.Json, it)
                finish()
            }
        }
    }
    install(Routing) {
        install(FreeMarker) {
            templateLoader = ClassTemplateLoader(Application::class.java.classLoader, "templates")
        }
        static {
            resources("public")
        }
        index()
        descriptor()
        file()
        qr()
        get()
        upload()
        share()
        privacy()
    }
}

fun main(args: Array<String>) {
    embeddedServer(Netty, Service.port, watchPaths = listOf("heroku"), module = Application::module).start()
}
