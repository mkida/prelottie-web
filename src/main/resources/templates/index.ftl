<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/stylesheets/dropzone.css" rel="stylesheet">
        <link href="/stylesheets/main.css" rel="stylesheet">
        <script src="/scripts/dropzone.js"></script>
        <script src="/scripts/main.js"></script>
    </head>
    <body>
        <div id="dropzone">
            <form action="/upload" class="dropzone needsclick dz-clickable width centered" id="dropzone">
                <div class="dz-message middle-cell needsclick logo">
                    <div class="hint">
                        <p>Drop your Bodymovin JSON or ZIP file here or click to upload.</p>
                    </div>
                </div>
            </form>
        </div>
        <#if sampleId??>
        <p>
            <center>
                <a href="/s/${sampleId}" target="_blank">Click here</a> to try out a sample animation.
            </center>
        </p>
        </#if>
        <div id="qr-code-wrapper" class="width centered">
            <div class="middle-cell">
                <img id="qr-code"><br />
                <div>Please scan this QR code using the App.</div>
            </div>
        </div>
        <div id="stores-container">
            <a href="https://play.google.com/store/apps/details?id=io.kida.lottieprelude" target="_blank">
                <img src="/images/google-play-badge.png" class="store-logo" />
            </a>
            <p>Google Play and the Google Play logo are trademarks of Google LLC.</p>
        </div>
        <#if id??>
        <script type="text/javascript">
            showCode(${id});
        </script>
        </#if>
    </body>
</html>