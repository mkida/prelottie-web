function qrcodewrapper() {
    return document.getElementById("qr-code-wrapper");
};
function qrcode() {
    return document.getElementById("qr-code");
};
function dropzone() {
    return document.getElementById("dropzone");
};
Dropzone.options.dropzone = {
    init: function() {
        this.on("addedfile", function(file) { 
            console.log("Added: " + JSON.stringify(file));
        });
        this.on("error", function (file, error) {
            console.log("Error: " + JSON.stringify(file) + ", " + JSON.stringify(error));
        });
        this.on("success", function (file, success) {
            window.location = '/s/' + success.details.session;
        });
    },
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 5, // MB
};
function showCode(code) {
    if (code !== null) {
        dropzone().style.display = 'none';
        qrcode().src = '/qr/' + code;
        qrcodewrapper().style.display = 'table';
    }
}