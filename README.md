# Prelottie Web App

## An App to preview Bodymovin / Lottie animations

![Logo](https://drive.google.com/uc?export=download&id=1D67WBfMFFw7qa5RWquhqwX5Jq2_JPsr4)

I've initially written this project to play around with Ktor, the Kotlin Web Framework, [you can try out Prelottie here](https://prelottie.apps.kida.io/). Make sure not to miss out on the [Android App here](https://github.com/kimar/prelottie-android).

## Requirements

* Heroku / Dokku / etc.
* Minio (Self-hosted S3 replacement, for AWS S3 see [this commit](https://github.com/kimar/prelottie-web/commit/c65263efde3ec4037cdcd67a7a3c20a48f287d9c).)

## Deployment

Set the following environment variables for the app:

**MINIO_ENDPOINT_URL** = Your Minio Installation

**MINIO_ACCESS_KEY** = Your Minio Access Key

**MINIO_ACCESS_SECRET** = Your Minio Access Secret

**AWS_S3_BUCKET** = The Bucket name on your Minio / AWS

**BASE_URL** = The URL your app is going to be exposed at

**PORT** (Optional) = The port the app should be exposed to (Default: 8080)

**SAMPLE_ID** (Optional) = An id of a sample animation (used on the web frontend)

With those variables set the app can be deployed on a Heroku(ish) environment out of the box. Have fun!

## License

See [LICENSE.md](LICENSE.md)
